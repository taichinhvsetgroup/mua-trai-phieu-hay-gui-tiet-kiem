# Nên gửi tiết kiệm hay đầu tư trái phiếu

Khi sở hữu, có trong tay một khoản tiền nhàn rỗi đủ lớn, con người hay hình thành một thói quen tạo ra nguồn thu nhập mới bằng cách đầu tư. Hai hình thức thường được sử dụng nhất đó là mua trái phiếu và gửi tiết kiệm. 